class Board(object):
    def __init__(self):
        self._board = ['-' for _ in range(9)]
        self._history = []
        
    def _move(self, action, take):
        if self._board[action] == '-':
            self._board[action] = take
            self._history.append((action, take))
            
b = Board()
print("Hello World!")